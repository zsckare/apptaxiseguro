package mx.freelancer.zsckare.taxiseguro.Vista;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

import mx.freelancer.zsckare.taxiseguro.R;

public class MaterialIntroActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFullscreen(true);

        super.onCreate(savedInstanceState);

        setSkipEnabled(true);
        setFinishEnabled(true);

        addSlide(new SimpleSlide.Builder()
                .title(R.string.title_1)
                .background(R.color.colorAccent)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.title_2)
                .description(R.string.description_1)
                .image(R.drawable.add_references)
                .background(R.color.colorAccent)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.title_3)
                .description(R.string.description_2)
                .image(R.drawable.waiting_for_driver)
                .background(R.color.colorAccent)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.title_4)
                .image(R.drawable.info)
                .background(R.color.colorAccent)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.title_5)
                .image(R.drawable.rating)
                .background(R.color.colorAccent)
                .build());


        addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        
    }
}
