package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by macbook on 20/03/16.
 */
public class Directorio {
    private String base,telefono;

    public Directorio(String base, String telefono) {
        this.base = base;
        this.telefono = telefono;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
