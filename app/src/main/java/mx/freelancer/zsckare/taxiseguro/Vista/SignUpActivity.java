package mx.freelancer.zsckare.taxiseguro.Vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;

import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.ClientModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignUpActivity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    private EditText name, lastname, email, password;
    MaterialDialog dialog;
    MaterialDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        builder= new MaterialDialog.Builder(SignUpActivity.this)
                .title("Espere por favor")
                .content("Creando cuenta")
                .progress(true, 0)
                .progressIndeterminateStyle(true);
        dialog=builder.build();

        name = (EditText)findViewById(R.id.name_sign_up);
        email = (EditText)findViewById(R.id.email_sign_up);
        lastname = (EditText)findViewById(R.id.lastname_sign_up);
        password = (EditText)findViewById(R.id.password_sign_up);

        Button btn = (Button)findViewById(R.id.button_sign_up);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String eml =  email.getText().toString();
                String pass = password.getText().toString();

                if (!eml.isEmpty() && !pass.isEmpty()){
                    dialog.show();
                    register();
                }

            }
        });
    }


    private void register(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Api api = adapter.create(Api.class);

        api.creatAccount(name.getText().toString(), lastname.getText().toString(), email.getText().toString(), password.getText().toString(), new Callback<ClientModel>() {
            @Override
            public void success(ClientModel clientModel, Response response) {
                dialog.dismiss();
                Log.d(TAG, "success: ----------------->");
                SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = pref.edit();

                editor.putString("Nombre", clientModel.getName());
                editor.putString("Paterno", clientModel.getPaterno());
                editor.putString("Materno", clientModel.getMaterno());
                editor.putString("correo", clientModel.getEmail());
                editor.putInt("ID_CLIENTE", clientModel.getId());
                editor.putInt("Status", 1);
                editor.apply();
                Intent intent = new Intent(SignUpActivity.this, DrawerActivity.class);
                startActivity(intent);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
