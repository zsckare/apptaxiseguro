package mx.freelancer.zsckare.taxiseguro.Vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.LinkedList;
import java.util.List;

import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.LocationModel;
import mx.freelancer.zsckare.taxiseguro.Models.ServicioModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import mx.freelancer.zsckare.taxiseguro.Utils.CustomListAdapter;
import mx.freelancer.zsckare.taxiseguro.Utils.FavoritesAdapter;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LocationsActivity extends AppCompatActivity {

    public static LinkedList<LocationModel> favoritesServices = new LinkedList<>();
    LinkedList<String>names = new LinkedList<>();
    ListView favorites;
    MaterialDialog dialog;
    MaterialDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        favorites = (ListView)findViewById(R.id.listViewFavorites);

        favorites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final LocationModel service = favoritesServices.get(position);
                Comun.favorite_position = position;
                new MaterialDialog.Builder(LocationsActivity.this)
                        .title(service.getName())
                        .content(service.getReferencias())
                        .positiveText("Pedir")
                        .negativeText("Cancelar")
                        .neutralText("Editar / Eliminar")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                orderServiceT(position);
                            }
                        })
                        .onNeutral(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                               detailsService(position);
                            }
                        })
                        .show();

            }
        });
        favorites.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                detailsService(position);
                return false;
            }
        });


        getMyLocations();
    }

    private void detailsService(int position) {
        LocationModel service = favoritesServices.get(position);
        new MaterialDialog.Builder(LocationsActivity.this)
                .title(service.getName())
                .content(service.getReferencias())
                .positiveText("Cancelar")
                .negativeText("Editar")
                .neutralText("Eliminar")
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Toast.makeText(LocationsActivity.this, "Eliminando Favorito", Toast.LENGTH_SHORT).show();
                        deleteFavorite();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        startActivity(new Intent(getApplicationContext(),EditLocationActivity.class));
                    }
                })
                .show();
    }

    private void showHelp() {
        new MaterialDialog.Builder(LocationsActivity.this)
                .title("Ayuda")
                .content("Para pedir un servicio desde favoritos, basta con tocar el elemento de la lista.\n Para editar o eliminar el favorito tienes que dejar presionado el elemento de la lista.")
                .positiveText("Aceptar")
                .show();
    }

    private void orderServiceT(int position) {
        LocationModel service = favoritesServices.get(position);
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();


        Api api = adapter.create(Api.class);

        api.orderService(service.getClient_id(), service.getReferencias(), service.getLatlng(), "2.0", new Callback<ServicioModel>() {
            @Override
            public void success(final ServicioModel servicioModel, Response response) {
                if (servicioModel.getLocked() == 0) {
                    Comun.chat_id = servicioModel.getChat();
                    builder = new MaterialDialog.Builder(LocationsActivity.this)
                            .title("Espere alrededor de dos minutos")
                            .content("Buscando una unidad disponible")
                            .progress(true, 0)
                            .canceledOnTouchOutside(false)
                            .positiveText("Cancelar Servicio")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();

                                    new MaterialDialog.Builder(LocationsActivity.this)
                                            .title("Cancelar Servicio")
                                            .content("¿Quieres Cancelar el servicio?")
                                            .positiveText("Si")
                                            .negativeText("No")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog ssss, @NonNull DialogAction which) {
                                                    cancelService(servicioModel.getId(), 3);
                                                }
                                            })
                                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dddd, @NonNull DialogAction which) {
                                                    dialog.show();
                                                }
                                            })
                                            .show();
                                }
                            })
                            .autoDismiss(false);

                    dialog = builder.build();
                    dialog.show();
                    SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("last", servicioModel.getId());
                    editor.putInt("chat_id", Comun.chat_id);
                    editor.apply();
                    Comun.latlng = servicioModel.getLatlon();
                    getServiceStatus(servicioModel.getId());
                }
                if (servicioModel.getLocked()==1){
                    new MaterialDialog.Builder(LocationsActivity.this)
                            .title("Lo Sentimos")
                            .content(R.string.time_expired)
                            .positiveText("Aceptar")
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(LocationsActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getServiceStatus(final int servicio_id){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.getService(servicio_id, new Callback<ServicioModel>() {
            @Override
            public void success(ServicioModel servicioModel, Response response) {

                if (servicioModel.getStatus() ==2){
                    Comun.driver_id = servicioModel.getDriver_id();

                    dialog.dismiss();

                    startActivity(new Intent(getApplicationContext(), DriverInfoActivity.class));
                }else if (servicioModel.getStatus()==3){

                }else {
                    getServiceStatus(servicio_id);
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void cancelService(int service_id, int status){
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.cancelService(service_id, status, new Callback<ServicioModel>() {
            @Override
            public void success(ServicioModel servicioModel, Response response) {
                Toast.makeText(LocationsActivity.this, "Servicio Cancelado", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(LocationsActivity.this, "Fallo =>"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getMyLocations() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();


        Api api = adapter.create(Api.class);
        api.getMyLocations(Comun.authentication_token, new Callback<List<LocationModel>>() {
            @Override
            public void success(List<LocationModel> locationModels, Response response) {
                if (!locationModels.isEmpty()){
                    //Toast.makeText(LocationsActivity.this, locationModels.toString(), Toast.LENGTH_SHORT).show();
                    for (LocationModel location:locationModels) {
                        favoritesServices.add(location);
                        names.add(location.getName());
                    }
                    fillList();
                }else{
                    new MaterialDialog.Builder(LocationsActivity.this)
                            .title("Favoritos")
                            .content("Al parecer no tienes ningun favorito guardado.")
                            .positiveText("Aceptar")
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                new MaterialDialog.Builder(LocationsActivity.this)
                        .title("Error")
                        .content(R.string.error_inesperado)
                        .positiveText("Aceptar")
                        .show();
            }
        });
    }

    private void fillList() {
        final FavoritesAdapter mLeadsAdapter = new FavoritesAdapter(this, names);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                favorites.setAdapter(mLeadsAdapter);
            }
        });
    }

    private void deleteFavorite(){
        LocationModel service = favoritesServices.get(Comun.favorite_position);
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();


        Api api = adapter.create(Api.class);
        api.deleteFavorite(service.getId(), new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                Toast.makeText(LocationsActivity.this, "Favorito Eliminado", Toast.LENGTH_SHORT).show();
                reload();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void reload() {
        favoritesServices.clear();
        names.clear();
        favorites.setAdapter(null);
        getMyLocations();

    }
}
