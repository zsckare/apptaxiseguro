package mx.freelancer.zsckare.taxiseguro.Models;

public class Rating {

    int id, service_id;

    String suggestions;

    public Rating(int service_id, String suggestions) {
        this.service_id = service_id;

        this.suggestions = suggestions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }



    public String getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }
}
