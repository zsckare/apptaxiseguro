package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by katty on 19/10/16.
 */
public class MessageModel {
    int id, chat_id,user_type;
    String message;

    public MessageModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageModel{" +
                "id=" + id +
                ", chat_id=" + chat_id +
                ", user_type=" + user_type +
                ", message='" + message + '\'' +
                '}';
    }
}
