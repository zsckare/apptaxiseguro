package mx.freelancer.zsckare.taxiseguro.Api;


import java.util.List;

import mx.freelancer.zsckare.taxiseguro.Models.ClientModel;
import mx.freelancer.zsckare.taxiseguro.Models.DistanceModel;
import mx.freelancer.zsckare.taxiseguro.Models.DriverModel;
import mx.freelancer.zsckare.taxiseguro.Models.HistorialModel;
import mx.freelancer.zsckare.taxiseguro.Models.LocationModel;
import mx.freelancer.zsckare.taxiseguro.Models.MessageModel;
import mx.freelancer.zsckare.taxiseguro.Models.ServicioModel;
import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface Api {


    @GET("/drivers")
    void getDrivers(Callback<List<DriverModel>> callback);

    @GET("/drivers/{id}")
    void getDriver(@Path("id") int id, Callback<DriverModel> response);

    @POST("/services")
    void orderService(@Query("service[client_id]")int client_id, @Query("service[referencia]")String references, @Query("service[latlon]")String latlng, @Query("app")String version, Callback<ServicioModel>callback);

    @GET("/services/{id}")
    void getService(@Path("id")int id, Callback<ServicioModel>callback);

    @PATCH("/services/{id}")
    void cancelService(@Path("id") int id, @Query("service[status]")int status, Callback<ServicioModel> callback);

    @GET("/me/historial")
    void getMyServices(@Query("authentication_token") String token, Callback<List<HistorialModel>>callback);

    @GET("/directionsmatrix/calculate")
    void getEstimateTime(@Query("origins")String origin, @Query("destine")String destine, Callback<DistanceModel>callback);

    @POST("/updater/update_token")
    void updateToken(@Query("id") int id,@Query("authentication_token") String token,Callback<ClientModel>callback);

    @GET("/sessionclient/swift_login")
    void getClientSesion(@Query("email") String email,@Query("password") String password, Callback<ClientModel> response);

    @GET("/sessionclient/social")
    void getFbLogin(@Query("email") String email,@Query("password") String password, Callback<ClientModel> response);
    @POST("/messages")
    void sendMessage(@Query("message[chat_id]")int chatId, @Query("message[message]")String msg, @Query("message[user_type]")int type, Callback<List<MessageModel>>callback);

    @GET("/messages")
    void getMessages(@Query("chat_id")int chatId,Callback<List<MessageModel>>callback);

    @POST("/recuperation/recuperation_mail")
    void recuperationPassword(@Query("email") String email, @Query("new_password") String new_password, Callback<ClientModel> callback );

    @POST("/locations")
    void saveService(@Query("authentication_token") String token, @Query("location[latlng]") String latlng, @Query("location[name]") String name, @Query("location[referencias]") String referencias, Callback<LocationModel>callback);

    @PATCH("/locations/{id}")
    void updateFavorite(@Path("id")int id, @Query("location[latlng]") String latlng, @Query("location[name]") String name, @Query("location[referencias]") String referencias, Callback<LocationModel>callback);

    @DELETE("/locations/{id}")
    void deleteFavorite(@Path("id")int id, Callback<Void>callback);
    @GET("/locations")
    void getMyLocations(@Query("authentication_token") String token,Callback<List<LocationModel>>callback);

    @POST("/clients")
    void creatAccount(@Query("client[name]")String name,@Query("client[paterno]")String lastname, @Query("client[email]")String email, @Query("client[password]")String password, Callback<ClientModel>clientModelCallback );
}
