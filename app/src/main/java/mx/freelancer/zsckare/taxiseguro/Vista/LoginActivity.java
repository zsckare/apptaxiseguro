package mx.freelancer.zsckare.taxiseguro.Vista;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.ClientModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Seguridad;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    private EditText emailTF,passwordTF;
    private Button btn;
    MaterialDialog dialog;
    MaterialDialog.Builder builder;
    Vibrator vibrator;
    LoginButton loginButton;
    CallbackManager callbackManager;

    String uid,name,lastname,email_,pass_,conf_pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_new_login);

        emailTF = (EditText)findViewById(R.id.emailLogin);
        passwordTF = (EditText)findViewById(R.id.passwordLogin);
        vibrator= (Vibrator)getSystemService(VIBRATOR_SERVICE);

        builder= new MaterialDialog.Builder(LoginActivity.this)
                .title("Iniciando Sesión")
                .content("Espere por favor")
                .progress(true, 0)
                .progressIndeterminateStyle(true);
        dialog=builder.build();

        btn = (Button)findViewById(R.id.buttonLogin);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(100);

                dialog.show();
                login();
            }
        });

        TextView label_recuperation = (TextView)findViewById(R.id.forgot_password_label);
        label_recuperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restoration_email_dialog();
            }
        });

        TextView sign_up_label = (TextView)findViewById(R.id.sign_up_label);
        sign_up_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SignUpActivity.class));
            }
        });
        if (Comun.logout==1){
            LoginManager.getInstance().logOut();
        }
        setUpFb();

        Button btnfb = (Button)findViewById(R.id.btn_fb_new);
        btnfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginButton.performClick();
            }
        });

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(R.string.denied_permission_message)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();

    }

    private void setUpFb() {
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile","email"));
        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.d(TAG, "onCompleted: "+object.toString());
                                try {
                                    email_ = object.getString("email");
                                    uid = ""+object.getInt("id");
                                    Profile profile = Profile.getCurrentProfile();
                                    String firstName = profile.getFirstName();
                                    lastname= profile.getLastName();
                                    name = firstName;

                                    fbLogin();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    private void login() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Api api = adapter.create(Api.class);
        String mail = emailTF.getText().toString();
        String psw = passwordTF.getText().toString();

        api.getClientSesion(mail, psw, new Callback<ClientModel>() {
            @Override
            public void success(ClientModel clientModel, Response response) {
                //Toast.makeText(LoginActivity.this, clientModel.toString(), Toast.LENGTH_SHORT).show();
                if (clientModel.getId() != 0){
                    dialog.dismiss();
                    Log.d(TAG, "success: ----------------->");
                    SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("Nombre",clientModel.getName());
                    editor.putString("Paterno",clientModel.getPaterno());
                    editor.putString("Materno",clientModel.getMaterno());
                    editor.putString("correo",clientModel.getEmail());
                    editor.putInt("ID_CLIENTE",clientModel.getId());
                    editor.putInt("Status",1);
                    editor.apply();
                    dialog.dismiss();
                    Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this, "Usuario y/o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dialog.dismiss();
                Toast.makeText(LoginActivity.this, getResources().getText(R.string.error_inesperado)+"\n("+error.getMessage()+")", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void fbLogin(){
        dialog.show();
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Api api = adapter.create(Api.class);

        String secure_pass = Seguridad.cifrar(uid);
        Log.d(TAG, "fbLogin: "+secure_pass);
        api.getFbLogin(email_, secure_pass, new Callback<ClientModel>() {
            @Override
            public void success(ClientModel clientModel, Response response) {
                if(clientModel != null){
                    dialog.dismiss();
                    Log.d(TAG, "success: ----------------->");
                    SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = pref.edit();

                    editor.putString("Nombre", clientModel.getName());
                    editor.putString("Paterno", clientModel.getPaterno());
                    editor.putString("Materno", clientModel.getMaterno());
                    editor.putString("correo", clientModel.getEmail());
                    editor.putInt("ID_CLIENTE", clientModel.getId());
                    editor.putInt("Status", 1);
                    editor.apply();
                    dialog.dismiss();
                    Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this, "No se ha encontrado una cuenta. \n Creando cuenta", Toast.LENGTH_SHORT).show();
                    register();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(LoginActivity.this, getResources().getText(R.string.error_inesperado)+"\n"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void register() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Api api = adapter.create(Api.class);

        api.creatAccount(name,lastname,email_,uid, new Callback<ClientModel>() {
            @Override
            public void success(ClientModel clientModel, Response response) {
                //dialog.dismiss();
                Log.d(TAG, "success: ----------------->");
                SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = pref.edit();

                editor.putString("Nombre", clientModel.getName());
                editor.putString("Paterno", clientModel.getPaterno());
                editor.putString("Materno", clientModel.getMaterno());
                editor.putString("correo", clientModel.getEmail());
                editor.putInt("ID_CLIENTE", clientModel.getId());
                editor.putInt("Status", 1);
                editor.commit();
                Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                startActivity(intent);

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    //--------------------------recuperacion de contraseña-----------------------------------------
    static String email_restoration = "";
    public void restoration_email_dialog(){
        new MaterialDialog.Builder(this)
                .title("Introduce el correo con el que te registraste")
                .input("Correo Electronico", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        email_restoration = input+"";
                        restoration_email();
                    }
                })
                .positiveText("Enviar").show();
    }

    private void restoration_email(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        Api api = adapter.create(Api.class);
        String new_pass = Comun.randomString(8);
        api.recuperationPassword(email_restoration, new_pass, new Callback<ClientModel>() {
            @Override
            public void success(ClientModel clientModel, Response response) {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title("Todo ha salido bien")
                        .content("En breve recibiras un correo con los nuevos datos para acceder")
                        .positiveText("Aceptar").show();
            }

            @Override
            public void failure(RetrofitError error) {
                new MaterialDialog.Builder(LoginActivity.this)
                        .title("Error")
                        .content(R.string.error_inesperado)
                        .positiveText("Aceptar").show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
