package mx.freelancer.zsckare.taxiseguro.Vista;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.ChatMessage;
import mx.freelancer.zsckare.taxiseguro.Models.DistanceModel;
import mx.freelancer.zsckare.taxiseguro.Models.DriverModel;
import mx.freelancer.zsckare.taxiseguro.Models.MessageModel;
import mx.freelancer.zsckare.taxiseguro.Models.ServicioModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import mx.freelancer.zsckare.taxiseguro.Utils.MessageAdapter;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PushDriverInfoActivity extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    static String fullname = "";
    // interfaz
    static String economico = "";
    private ImageView profileDriver;
    private TextView labelNombre, labelEconomico, labelAuto;
    GoogleMap mMap;
    CameraUpdate cam;
    MaterialDialog dialog;
    MaterialDialog.Builder builder;
    //-------
    boolean firstload = false;
    // chat
    boolean isMine = true;
    private List<ChatMessage> chatMessages ;
    private ArrayAdapter<ChatMessage> adapter;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_driver_info);

        initialize();
    }

    private void initialize() {
        profileDriver = (ImageView)findViewById(R.id.imgProfileDriver);
        labelNombre = (TextView)findViewById(R.id.label_nombre);
        labelEconomico = (TextView)findViewById(R.id.label_economico);
        listView = (ListView) findViewById(R.id.list_msg);
        labelAuto = (TextView)findViewById(R.id.label_carro);

        //chat---------------------------------------------------------
        chatMessages = new ArrayList<>();
        final EditText messageInput = (EditText)findViewById(R.id.msg_type);
        Button btnSend = (Button)findViewById(R.id.btn_chat_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = messageInput.getText().toString();
                if (!msg.trim().isEmpty()) {
                    messageInput.setText("");
                    sendMessage(msg);
                }
            }
        });

        SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

        int last = pref.getInt("last",0);
        Comun.service_id = last;
        Comun.chat_id = pref.getInt("chat_id",0);

        getMessages();
        builder = new MaterialDialog.Builder(this)
                .content("Cargando.")
                .progress(true, 0);
        dialog = builder.build();

        Button finalizar = (Button) findViewById(R.id.btnEndService);
        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RatingActivity.class));
            }
        });

        getServiceStatus(Comun.service_id);
    }

    private void getServiceStatus(final int servicio_id){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.getService(servicio_id, new Callback<ServicioModel>() {
            @Override
            public void success(ServicioModel servicioModel, Response response) {

                if (servicioModel.getStatus() ==2){
                    Comun.driver_id = servicioModel.getDriver_id();

                    getDriverInfo();
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getDriverInfo() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.getDriver(Comun.driver_id, new Callback<DriverModel>() {
            @Override
            public void success(DriverModel driverModel, Response response) {
                //Toast.makeText(DriverInfoActivity.this, ""+driverModel.toString(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "success: "+driverModel.toString());
                if (firstload == false) {
                    fullname = driverModel.getName() + " " + driverModel.getPaterno() + " " + driverModel.getMaterno();
                    labelNombre.setText(fullname);
                    labelEconomico.setText("" + driverModel.getEconomico());
                    labelAuto.setText(driverModel.getCar());
                    economico = driverModel.getEconomico();
                    Picasso.with(getApplicationContext()).load(driverModel.getAvatar()).into(profileDriver);
                    getEstimateTime(driverModel.getLatlon());
                    firstload = true;
                }else{

                    repaintMap(driverModel.getLatlon(), driverModel.getEconomico());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                getDriverInfo();
            }
        });
    }


    private void getEstimateTime( final String driver_latlng){
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();
        Api api = adapter.create(Api.class);

        String origin = Comun.latlng;
        api.getEstimateTime(origin, driver_latlng, new Callback<DistanceModel>() {
            @Override
            public void success(DistanceModel distanceModel, Response response) {
                dialog.dismiss();
                new MaterialDialog.Builder(PushDriverInfoActivity.this)
                        .title("Tiempo estimado de llegada.")
                        .positiveColor(getResources().getColor(R.color.colorPrimary))
                        .content(distanceModel.getText())
                        .positiveText(R.string.txt_aceptar)
                        .show();
                redrawMap(driver_latlng,economico);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }


    private void redrawMap(String driver_latlng, String economico){

        String latlngs[] = driver_latlng.split(",");

        float lat = Float.parseFloat(latlngs[0]);
        float lng = Float.parseFloat(latlngs[1]);

        String current_latlng [] = Comun.latlng.split(",");
        float me_lat = Float.parseFloat(current_latlng[0]);
        float me_lng = Float.parseFloat(current_latlng[1]);

        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapGo);
            mMap = mapFragment.getMap();
            if (mMap != null) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }

                cam = CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 18);
                mMap.animateCamera(cam);
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat,lng))
                        .title(economico)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_taxi))
                        .draggable(true)
                );

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(me_lat,me_lng))
                        .title("Mi Ubicacion")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_person))
                        .draggable(true)
                );


                Thread timerThread = new Thread(){
                    public void run(){
                        try{
                            sleep(3000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }finally{

                            getDriverInfo();

                        }
                    }
                };
                timerThread.start();

            }
        }


    }

    private void repaintMap(String driver_latlng, String economico){

        String latlngs[] = driver_latlng.split(",");

        float lat = Float.parseFloat(latlngs[0]);
        float lng = Float.parseFloat(latlngs[1]);

        String current_latlng [] = Comun.latlng.split(",");
        float me_lat = Float.parseFloat(current_latlng[0]);
        float me_lng = Float.parseFloat(current_latlng[1]);

        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat,lng))
                .title(economico)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_taxi))
                .draggable(true)
        );

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(me_lat,me_lng))
                .title("Mi Ubicacion")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_person))
                .draggable(true)
        );

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Log.d(TAG, "run: ---------------->");
                    getDriverInfo();

                }
            }
        };
        timerThread.start();
    }


    private void getMessages(){
        int chat = Comun.chat_id;
        RestAdapter adapter = new RestAdapter.Builder()

                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);

        api.getMessages(chat, new Callback<List<MessageModel>>() {
            @Override
            public void success(List<MessageModel> messageModels, Response response) {
                if(!chatMessages.isEmpty()){
                    chatMessages.clear();
                }
                if (!messageModels.isEmpty()){

                    for (MessageModel message:messageModels) {
                        Log.d(TAG, "success: "+message.toString());
                        if (message.getUser_type()==1){
                            Log.d(TAG, " =============>");
                            ChatMessage chatMessage = new ChatMessage(message.getMessage(),false);
                            chatMessages.add(chatMessage);
                        }else{
                            ChatMessage chatMessage = new ChatMessage(message.getMessage(),true);
                            chatMessages.add(chatMessage);
                        }
                    }

                    fillMessagesView();

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getMessages();
            }
        }, 2000);
    }
    private void sendMessage(String msg){
        int chat = Comun.chat_id;
        RestAdapter adapter = new RestAdapter.Builder()

                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.sendMessage(chat, msg, 1, new Callback<List<MessageModel>>() {
            @Override
            public void success(List<MessageModel> messageModels, Response response) {
                if(!chatMessages.isEmpty()){
                    chatMessages.clear();
                }
                if (!messageModels.isEmpty()){

                    for (MessageModel message:messageModels) {
                        Log.d(TAG, "success: "+message.toString());
                        if (message.getUser_type()==1){
                            Log.d(TAG, " =============>");
                            ChatMessage chatMessage = new ChatMessage(message.getMessage(),false);
                            chatMessages.add(chatMessage);
                        }else{
                            ChatMessage chatMessage = new ChatMessage(message.getMessage(),true);
                            chatMessages.add(chatMessage);
                        }
                    }

                    fillMessagesView();

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void fillMessagesView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter = new MessageAdapter(PushDriverInfoActivity.this, R.layout.item_chat_left, chatMessages);
                listView.setAdapter(adapter);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
