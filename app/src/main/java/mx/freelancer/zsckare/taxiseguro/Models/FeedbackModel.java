package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by katty on 02/10/16.
 */
public class FeedbackModel {
    int id;
    String comment;

    public FeedbackModel() {
    }

    public FeedbackModel(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
