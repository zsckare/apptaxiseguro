package mx.freelancer.zsckare.taxiseguro;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    String TAG = getClass().getSimpleName();
    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();

       //GeneralSettings.token=token;

        showToken();
    }

    private void showToken() {
       //Log.d(TAG, "showToken: ----------------->"+GeneralSettings.token);
    }


}
