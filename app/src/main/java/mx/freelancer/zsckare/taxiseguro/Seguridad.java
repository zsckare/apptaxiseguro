package mx.freelancer.zsckare.taxiseguro;


import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Seguridad {


    public static String MD5 = "MD5";
    public static String SHA256 = "SHA-256";


    public Seguridad(){}

    private static String toHexadecimal(byte[] digest){
        String hash = "";
        for(byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) hash += "0";
            hash += Integer.toHexString(b);
        }
        return hash;
    }

    public static String getStringMessageDigest(String message, String algorithm){
        byte[] digest = null;
        byte[] buffer = message.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
            Log.e("Error","Error creando Digest");
        }
        return toHexadecimal(digest);
    }

    public static String getToken(String str){
        String tkn;
        String clvprivada ="@aplication_wcode(encrypt)@";
        String id = clvprivada+str;
        tkn= getStringMessageDigest(id,SHA256);
        return tkn;
    }

    public static int verifySesion(int estado){
        int status = estado;
        return status;
    }

    final static String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }


    public static String cifrar(String password){
        String cifrado="";
        String encpriv = Seguridad.getStringMessageDigest("@aplication_wcode(encrypt)@",Seguridad.MD5);

        String val=encpriv+password;

        String cod = Seguridad.getStringMessageDigest(val,Seguridad.MD5);
        cifrado = Seguridad.getStringMessageDigest(cod,Seguridad.SHA256);


        return cifrado;
    }
}
