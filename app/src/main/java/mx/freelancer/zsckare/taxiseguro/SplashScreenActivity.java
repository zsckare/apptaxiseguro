package mx.freelancer.zsckare.taxiseguro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.vision.text.Text;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import mx.freelancer.zsckare.taxiseguro.Vista.DrawerActivity;
import mx.freelancer.zsckare.taxiseguro.Vista.LoginActivity;

public class SplashScreenActivity extends AppCompatActivity {
    boolean internetStatus =true ;
    boolean gpsStatus = true;

    static int estado = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
        estado = pref.getInt("Status",-1);
        //Toast.makeText(this, "Status==>"+estado, Toast.LENGTH_SHORT).show();


        ImageView image_splash = (ImageView)findViewById(R.id.img_splash);
        ImageView geekLogo = (ImageView)findViewById(R.id.geeklogo);
        final TextView txt = (TextView)findViewById(R.id.imgTetxView);

            Picasso.with(getApplicationContext()).load(R.drawable.logo).into(image_splash);
            Picasso.with(getApplicationContext()).load(R.drawable.geeklab).into(geekLogo, new Callback() {
                @Override
                public void onSuccess() {
                    txt.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {

                }
            });

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    if (estado ==-1){
                        Intent intent = new Intent(SplashScreenActivity.this,LoginActivity.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(SplashScreenActivity.this,DrawerActivity.class);
                        startActivity(intent);
                    }

                }
            }
        };
        timerThread.start();

    }



}
