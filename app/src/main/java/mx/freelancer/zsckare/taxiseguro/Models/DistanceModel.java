package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by katty on 26/09/16.
 */
public class DistanceModel {
    String text,value;

    public DistanceModel(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }



    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DistanceModel{" +
                "text='" + text + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
