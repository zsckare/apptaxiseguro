package mx.freelancer.zsckare.taxiseguro.Vista;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.location.Address;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;


import net.alhazmy13.catcho.library.Catcho;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;


import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.ClientModel;
import mx.freelancer.zsckare.taxiseguro.Models.DriverModel;
import mx.freelancer.zsckare.taxiseguro.Models.LocationModel;
import mx.freelancer.zsckare.taxiseguro.Models.ServicioModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Seguridad;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //interfaz ----------------------------------------
    private Button btnPedir;
    private EditText referencesField;
    private ImageView newBtn;
    GoogleMap mMap;
    CameraUpdate cam;
    MaterialDialog dialog;
    MaterialDialog.Builder builder;
    //--------------------------------------------------
    boolean internetStatus = true;
    boolean gpsStatus = true;
    Vibrator vibrator;
    String TAG = getClass().getSimpleName();
    //--------------------------------------------------
    //parte del core    --------------------------------
    static int user_id = 0;
    static int first_load = 0;
    static String latlng = "";
    static String references = "";
    LatLng current_position;
    //-------------------------------------------------
    static LinkedList<DriverModel>drivers = new LinkedList<>();
    //--------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Thread.setDefaultUncaughtExceptionHandler(new Catcho.Builder(this).recipients("alianzagodgo@gmail.com").build());
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                setUpMapIfNeeded();
                internetStatus = isNetworkAvailable(getApplicationContext());
                LocationManager mlocManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                gpsStatus = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            }


        };

        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(R.string.denied_permission_message)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();

        first_load = 0;
        initializaInterfaz();
    }

    private  void initializaInterfaz(){

        // instanciar objetos

        referencesField = (EditText)findViewById(R.id.tV_referencias);
        //btnPedir = (Button) findViewById(R.id.btnPedirr);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        newBtn = (ImageView)findViewById(R.id.new_button_img);
        /*btnPedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int referencesStatus = checkReferences();

                references = referencesField.getText().toString();

                    if (referencesStatus == 1) {
                        vibrator.vibrate(100);
                        orderService();
                    }else {
                        Snackbar.make(v, "Las referencias deben de tener un tamaño de 20 caracteres", Snackbar.LENGTH_SHORT).show();
                    }



            }
        });
*/
        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int referencesStatus = checkReferences();

                references = referencesField.getText().toString();

                if (referencesStatus == 1) {
                    vibrator.vibrate(100);

                    askToSave();
                    //orderService();
                }else {
                    Snackbar.make(v, "Las referencias deben de tener un tamaño de 20 caracteres", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LocationsActivity.class));
            }
        });
        getPreferences();

    }

    private void getPreferences() {
        SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

        user_id = pref.getInt("ID_CLIENTE", 0);
        Log.d(TAG, "getPreferences: "+user_id);
        String token = pref.getString("token", "token");
        String tk = FirebaseInstanceId.getInstance().getToken();
        if (token.compareToIgnoreCase(tk) != 0) {
            updateToken(tk);
        }
    }

    private void updateToken(String tk) {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Api api = adapter.create(Api.class);
        api.updateToken(user_id, tk, new Callback<ClientModel>() {
            @Override
            public void success(ClientModel clientModel, Response response) {
                Comun.authentication_token = clientModel.getAuthentication_token();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void setUpMapIfNeeded() {

        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapadrawer);
            mMap = mapFragment.getMap();
            if (mMap != null) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        double lat = location.getLatitude();
                        double lng = location.getLongitude();

                        if (first_load == 0){
                            first_load = 1;
                            latlng = lat+","+lng;
                            cam = CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 18);
                            mMap.animateCamera(cam);
                            String address = getCompleteAddressString(lat,lng);
                            referencesField.setText(address);
                            getDrivers();
                        }
                    }
                });




                mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        current_position = cameraPosition.target;
                        double lat = current_position.latitude;
                        double lng = current_position.longitude;
                        latlng = lat+","+lng;
                        String address = getCompleteAddressString(current_position.latitude,current_position.longitude);
                        referencesField.setText(address);
                    }
                });
            }
        }
    }
//choferes en el mapa

    private void getDrivers(){
        RestAdapter adapter = new RestAdapter.Builder()

                .setEndpoint(Comun.BASE_URL)
                .build();


        Api api = adapter.create(Api.class);

        api.getDrivers(new Callback<List<DriverModel>>() {
            @Override
            public void success(List<DriverModel> driverModels, Response response) {
                if (!drivers.isEmpty()){
                    drivers.clear();

                }
                if (!driverModels.isEmpty()){
                    for (DriverModel driver:driverModels) {
                        drivers.add(driver);
                    }
                    drawDrivers();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void drawDrivers() {
        mMap.clear();

        for (DriverModel driver : drivers) {
            String latlngs[] = driver.getLatlon().split(",");
            double lat = Double.parseDouble(latlngs[0]);
            double lng = Double.parseDouble(latlngs[1]);
            LatLng ltlngChofer = new LatLng(lat,lng);

            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_taxi))
                    .title(driver.getEconomico())
                    .position(ltlngChofer)
            );
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getDrivers();
            }
        }, 2500);
    }

    //google
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("Myaddress", "" + strReturnedAddress.toString());
            } else {
                Log.w("Myaddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Myaddress", "Canont get Address!");
        }
        return strAdd;
    }

    // sobreecritos
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_historial){
            startActivity(new Intent(getApplicationContext(), HistorialActivity.class));
        }
        if (id == R.id.menu_favorites){
            startActivity(new Intent(getApplicationContext(), LocationsActivity.class));
        }
        if (id == R.id.cerrarsesion){
            cerrarSesion();
        }
        if (id == R.id.myaccount){
            startActivity(new Intent(getApplicationContext(), MyAccountActivity.class));
        }
        if(id == R.id.nav_share){
            startActivity(new Intent(getApplicationContext(), AboutActivity.class));
        }
        if(id == R.id.tutorial){
            startActivity(new Intent(getApplicationContext(), MaterialIntroActivity.class));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    // metodos para los servicios

    private void orderService(){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);

        api.orderService(user_id, references, latlng, "2", new Callback<ServicioModel>() {
            @Override
            public void success(final ServicioModel servicioModel, Response response) {

                if (servicioModel.getLocked() == 0){
                    Comun.chat_id = servicioModel.getChat();
                    builder = new MaterialDialog.Builder(DrawerActivity.this)
                            .title("Espere alrededor de dos minutos")
                            .content("Buscando una unidad disponible")
                            .progress(true, 0)
                            .positiveText("Cancelar Servicio")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull final MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();

                                    new MaterialDialog.Builder(DrawerActivity.this)
                                            .title("Cancelar Servicio")
                                            .content("¿Quieres Cancelar el servicio?")
                                            .positiveText("Si")
                                            .negativeText("No")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog ssss, @NonNull DialogAction which) {
                                                    cancelService(servicioModel.getId(),3);
                                                }
                                            })
                                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dddd, @NonNull DialogAction which) {
                                                    dialog.show();
                                                }
                                            })
                                            .show();
                                }
                            })
                            .autoDismiss(false);

                    dialog = builder.build();
                    dialog.show();
                    SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("last",servicioModel.getId());
                    editor.putInt("chat_id",Comun.chat_id);
                    editor.apply();
                    Comun.latlng = servicioModel.getLatlon();
                    getServiceStatus(servicioModel.getId());
                }
                if (servicioModel.getLocked()==1){
                    new MaterialDialog.Builder(DrawerActivity.this)
                            .title("Lo Sentimos")
                            .content(R.string.time_expired)
                            .positiveText("Aceptar")
                            .show();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getServiceStatus(final int servicio_id){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.getService(servicio_id, new Callback<ServicioModel>() {
            @Override
            public void success(ServicioModel servicioModel, Response response) {

                if (servicioModel.getStatus() ==2){
                    Comun.driver_id = servicioModel.getDriver_id();

                    dialog.dismiss();

                    startActivity(new Intent(getApplicationContext(), DriverInfoActivity.class));
                }else if (servicioModel.getStatus()==3){

                }else {
                    getServiceStatus(servicio_id);
                }


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void cancelService(int service_id, int status){
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();

        Api api = adapter.create(Api.class);
        api.cancelService(service_id, status, new Callback<ServicioModel>() {
            @Override
            public void success(ServicioModel servicioModel, Response response) {
                Toast.makeText(DrawerActivity.this, "Servicio Cancelado", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(DrawerActivity.this, "Fallo =>"+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void cerrarSesion(){
        Comun.logout=1;
        SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

        int estado = pref.getInt("Status",-1);
        //Toast.makeText(this, "Cerrando sesion "+estado, Toast.LENGTH_SHORT).show();
        if (Seguridad.verifySesion(estado)==1){
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("Status",0);
            editor.apply();
            Intent intent = new Intent(DrawerActivity.this,LoginActivity.class);
            startActivity(intent);

        }
    }
    // favoritos

    private void askToSave(){
        new MaterialDialog.Builder(DrawerActivity.this)
                .title("Favoritos")
                .content("¿Quieres guardar el servicio en favoritos?")
                .positiveText("Si")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new MaterialDialog.Builder(DrawerActivity.this)
                                .title("Guardar Servicio")
                                .content("Nombre del Servicio")
                                .inputType(InputType.TYPE_CLASS_TEXT)
                                .input("Ej. Trabajo", null, new MaterialDialog.InputCallback() {
                                    @Override
                                    public void onInput(MaterialDialog dialog, CharSequence input) {
                                       saveService(input.toString());
                                    }
                                }).show();
                    }
                })
                .negativeText("No")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        orderService();
                    }
                })
                .show();
    }

    private void saveService(String name_location){

        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();


        Api api = adapter.create(Api.class);
        api.saveService(Comun.authentication_token, latlng, name_location, references, new Callback<LocationModel>() {
            @Override
            public void success(LocationModel locationModel, Response response) {
                Toast.makeText(DrawerActivity.this, "Servicio Guardado", Toast.LENGTH_SHORT).show();
                orderService();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private int checkReferences() {
        int ref = 0;

        int tot = referencesField.getText().length();
        if (tot >= 10) {
            ref = 1;
        }

        return ref;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
