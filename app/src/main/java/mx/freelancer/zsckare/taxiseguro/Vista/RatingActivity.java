package mx.freelancer.zsckare.taxiseguro.Vista;

import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import net.alhazmy13.catcho.library.Catcho;

import mx.freelancer.zsckare.taxiseguro.Models.Rating;
import mx.freelancer.zsckare.taxiseguro.Models.Score;
import mx.freelancer.zsckare.taxiseguro.R;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RatingActivity extends AppCompatActivity {

    private RatingBar ratingBar;
    private TextView ratingValue;
    private EditText sugestion;
    private Button button, btnNotNow;
    private static float ratings;
    private static String suggestion;

    Vibrator vibrator;

    MaterialDialog dialog;
    MaterialDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Catcho.Builder(this).recipients("tokyo.blue.tsubasa@gmail.com").build());
        setContentView(R.layout.activity_rating);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        vibrator= (Vibrator)getSystemService(VIBRATOR_SERVICE);
        addListenerOnRatingBar();
        addListenerOnButton();

    }



    public void addListenerOnRatingBar() {

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setRating(3);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            public void onRatingChanged(RatingBar ratingBar, float rating,	boolean fromUser) {

                ratings = rating;
                //Toast.makeText(RatingActivity.this, ""+ String.valueOf(rating), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void addListenerOnButton() {
        sugestion = (EditText)findViewById(R.id.editTextSuggestion);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        button = (Button) findViewById(R.id.button);
        btnNotNow = (Button)findViewById(R.id.btn_not_now);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                vibrator.vibrate(50);
                suggestion= sugestion.getText().toString();
                int punt = Math.round(ratings);
               // createRating(GeneralSettings.SERVICE_ID,punt,suggestion);
                redirectHome();

            }

        });

        btnNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(50);
                suggestion= sugestion.getText().toString();
                //createRating(GeneralSettings.SERVICE_ID,5,"");
                redirectHome();
            }
        });

    }

    private void createRating(int idService, int puntuacion, String comment) {
/*
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(GeneralSettings.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Rating rating = new Rating(GeneralSettings.SERVICE_ID,suggestion);
        RatingsApi api = adapter.create(RatingsApi.class);

        api.createRating(idService, puntuacion, comment, new Callback<Score>() {
            @Override
            public void success(Score score, Response response) {
                vibrator.vibrate(50);
                redirectHome();
            }

            @Override
            public void failure(RetrofitError error) {

                redirectHome();
            }
        });
        */

    }

    public void redirectHome(){
        Intent intent = new Intent(RatingActivity.this,DrawerActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in,R.anim.right_out);
    }
}
