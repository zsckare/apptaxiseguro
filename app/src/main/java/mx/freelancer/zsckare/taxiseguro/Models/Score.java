package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by katty on 02/10/16.
 */
public class Score {

    int id, id_service, puntuacion;
    String comments;

    public Score(int id_service, int puntuacion, String comments) {
        this.id_service = id_service;
        this.puntuacion = puntuacion;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_service() {
        return id_service;
    }

    public void setId_service(int id_service) {
        this.id_service = id_service;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
