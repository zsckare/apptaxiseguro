package mx.freelancer.zsckare.taxiseguro;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import br.com.goncalves.pugnotification.notification.PugNotification;
import mx.freelancer.zsckare.taxiseguro.Vista.DriverInfoActivity;
import mx.freelancer.zsckare.taxiseguro.Vista.PushDriverInfoActivity;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    String TAG = getClass().getSimpleName();
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String msg = "";
        Log.d(TAG, "onMessageReceived: "+remoteMessage.getNotification().toString());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if remoteMessage.getNotification() == null ??
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.d(TAG, ">>>>>>>>>>>>>>>>>: "+remoteMessage.getData().get("message"));
        msg += remoteMessage.getNotification().getBody();

        if (!msg.isEmpty()){
            showNotification(msg);
        }
    }

    private void showNotification(String msg){
        Log.d(TAG, "showNotification: "+msg);

        PugNotification.with(getApplicationContext())
                .load()
                .autoCancel(true)
                .title("Vendra por usted")
                .message(msg)
                .bigTextStyle(msg)

                .smallIcon(R.drawable.logo)
                .largeIcon(R.drawable.logo)
                .flags(Notification.DEFAULT_ALL)
                .simple()
                .build();

    }

    public static void showNotificationll(Context context){


        Log.d("Fire", "showNotificationll: ---------->");
        Intent intent = new Intent(context, PushDriverInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code*/ , intent,
                PendingIntent.FLAG_ONE_SHOT);

        PugNotification.with(context)
                .load()
                .autoCancel(true)
                .title("AlianzaGO")
                .message("Un chofer vendra por usted")
                .bigTextStyle("Un chofer vendra por usted")
                .click(PushDriverInfoActivity.class)
                .smallIcon(R.drawable.logo)
                .largeIcon(R.drawable.logo)
                .flags(Notification.DEFAULT_ALL)
                .simple()
                .build();


    }
}
