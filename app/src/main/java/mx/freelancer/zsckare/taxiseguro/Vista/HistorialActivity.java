package mx.freelancer.zsckare.taxiseguro.Vista;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.LinkedList;
import java.util.List;

import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.HistorialModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import mx.freelancer.zsckare.taxiseguro.Utils.CustomListAdapter;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HistorialActivity extends AppCompatActivity {

    String TAG = getClass().getSimpleName();
    public static LinkedList<HistorialModel>historialServices = new LinkedList<>();
    static  LinkedList<String>list_service_references = new LinkedList<>();
    private ListView listViewServices;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        listViewServices = (ListView)findViewById(R.id.listViewServices);
        listViewServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getInfoFromService(position);
            }
        });
        getMyServices();
    }

    private void getInfoFromService(int position) {
        HistorialModel service = historialServices.get(position);

        String driver_name = service.getDriver_name()+" "+service.getDriver_paterno()+ " "+service.getDriver_materno();

        String content = driver_name + "\n" + service.getDriver_economico();
        //Toast.makeText(this, driver_name, Toast.LENGTH_SHORT).show();
        new MaterialDialog.Builder(HistorialActivity.this)
                .title("Servicio Cubierto")
                .content(content)
                .positiveText("Aceptar")
                .show();
    }

    private void getMyServices() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Comun.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        Api api = adapter.create(Api.class);
        api.getMyServices(Comun.authentication_token, new Callback<List<HistorialModel>>() {
            @Override
            public void success(List<HistorialModel> historialModels, Response response) {

                if (!historialServices.isEmpty()){
                    historialServices.clear();
                    list_service_references.clear();
                }
                if (!historialModels.isEmpty()) {
                    for (HistorialModel service : historialModels) {
                        historialServices.add(service);
                        Log.d(TAG, "success: "+service.getDate());
                        list_service_references.add(service.getReferencia());
                    }
                    //Toast.makeText(HistorialActivity.this, ""+historialModels.size(), Toast.LENGTH_SHORT).show();
                    fillListVew();
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void fillListVew() {
        //Toast.makeText(this, "---", Toast.LENGTH_SHORT).show();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, list_service_references);
        final CustomListAdapter mLeadsAdapter = new CustomListAdapter(this, list_service_references);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                listViewServices.setAdapter(mLeadsAdapter);
            }
        });
    }
}
