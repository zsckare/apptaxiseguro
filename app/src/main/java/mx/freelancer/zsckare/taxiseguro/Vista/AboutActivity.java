package mx.freelancer.zsckare.taxiseguro.Vista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import net.alhazmy13.catcho.library.Catcho;

import mx.freelancer.zsckare.taxiseguro.R;

public class AboutActivity extends AppCompatActivity {

    Button btnTutorial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Catcho.Builder(this).recipients("alianzagodgo@gmail.com").build());
        setContentView(R.layout.activity_about);

        btnTutorial = (Button)findViewById(R.id.btn_tutorial);

        btnTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutActivity.this, MaterialIntroActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in,R.anim.right_out);
    }
}
