package mx.freelancer.zsckare.taxiseguro.Vista;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.alhazmy13.catcho.library.Catcho;

import mx.freelancer.zsckare.taxiseguro.R;

public class CommentActivity extends AppCompatActivity {


    static int idClient = 0;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Catcho.Builder(this).recipients("tokyo.blue.tsubasa@gmail.com").build());
        setContentView(R.layout.activity_comment);


        editText = (EditText)findViewById(R.id.editTextComments);



        Button btn = (Button)findViewById(R.id.btnComment);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comments = editText.getText().toString();

                if (comments.trim().isEmpty()){
                    Toast.makeText(CommentActivity.this, "Los comentarios no pueden estar vacios", Toast.LENGTH_SHORT).show();
                }else {
                    sendFeedback(comments);
                }



            }
        });



    }

    private void getPreferencias() {
        String n, p, m;

        SharedPreferences pref = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

        idClient = pref.getInt("ID_CLIENTE", 0);

    }

    private void sendFeedback(String feedback){

        /*RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(GeneralSettings.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        FeedbackModel feedbackModel = new FeedbackModel(feedback);
        RatingsApi api = adapter.create(RatingsApi.class);


        api.createFeedback(feedback,"android",idClient, new Callback<FeedbackModel>() {
            @Override
            public void success(FeedbackModel feedbackModel, Response response) {

                editText.setText("");
                new MaterialDialog.Builder(CommentActivity.this)
                        .title("Se ha enviado correctamente")
                        .content("Agradecemos tus comentarios")
                        .positiveText("Aceptar")
                        .show();


            }

            @Override
            public void failure(RetrofitError error) {
                new MaterialDialog.Builder(CommentActivity.this)
                        .title("Algo ha salido mal")
                        .content(R.string.error_inesperado)
                        .positiveText("Aceptar")
                        .show();
            }
        });*/
    }
}
