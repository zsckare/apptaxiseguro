package mx.freelancer.zsckare.taxiseguro.Vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import net.alhazmy13.catcho.library.Catcho;


import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Seguridad;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyAccountActivity extends AppCompatActivity {

    private EditText editTextNombre,editTextPaterno,editTextMaterno,editTextEmail;

    private Button btn_update, btn_delete;

    Vibrator vibrator;

    MaterialDialog dialog;
    MaterialDialog.Builder builder;

    public int id_client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Catcho.Builder(this).recipients("tokyo.blue.tsubasa@gmail.com").build());
        setContentView(R.layout.activity_my_account);


        editTextNombre = (EditText)findViewById(R.id.nombre_edit);
        editTextPaterno = (EditText)findViewById(R.id.paterno_edit);
        editTextMaterno = (EditText)findViewById(R.id.materno_edit);
        editTextEmail = (EditText)findViewById(R.id.email_edit);

        btn_update = (Button)findViewById(R.id.btn_update_info);
        btn_delete = (Button)findViewById(R.id.btn_delete_account);

        vibrator= (Vibrator)getSystemService(VIBRATOR_SERVICE);



    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in,R.anim.right_out);
    }
}

