package mx.freelancer.zsckare.taxiseguro.Models;

import java.util.List;

/**
 * Created by macbook on 09/03/16.
 */
public class DriverModel {
    int driver_id;
    String name;
    String paterno;
    String materno;
    String email;
    String password;
    String latlon;
    String economico;
    String car;
    String avatar;

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLatlon() {
        return latlon;
    }

    public void setLatlon(String latlon) {
        this.latlon = latlon;
    }

    public String getEconomico() {
        return economico;
    }

    public void setEconomico(String economico) {
        this.economico = economico;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "DriverModel{" +
                "driver_id=" + driver_id +
                ", name='" + name + '\'' +
                ", paterno='" + paterno + '\'' +
                ", materno='" + materno + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", latlon='" + latlon + '\'' +
                ", economico='" + economico + '\'' +
                ", car='" + car + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
