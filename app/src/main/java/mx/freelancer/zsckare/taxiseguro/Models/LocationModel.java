package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by katty on 26/01/17.
 */

public class LocationModel {
    int id,client_id;
    String latlng, referencias, name;

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getReferencias() {
        return referencias;
    }

    public void setReferencias(String referencias) {
        this.referencias = referencias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "LocationModel{" +
                "id=" + id +
                ", client_id=" + client_id +
                ", latlng='" + latlng + '\'' +
                ", referencias='" + referencias + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
