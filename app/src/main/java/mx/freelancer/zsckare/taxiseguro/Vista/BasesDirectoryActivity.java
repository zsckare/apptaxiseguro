package mx.freelancer.zsckare.taxiseguro.Vista;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.alhazmy13.catcho.library.Catcho;

import mx.freelancer.zsckare.taxiseguro.Models.Directorio;
import mx.freelancer.zsckare.taxiseguro.R;

public class BasesDirectoryActivity extends AppCompatActivity {

    TextView tv_Paseo, tv_87, tv_ecoradios, tv_central;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new Catcho.Builder(this).recipients("alianzagodgo@gmail.com").build());
        setContentView(R.layout.activity_bases_directory);

        tv_87 = (TextView) findViewById(R.id.tv_87);
        tv_87.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "8101805";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(BasesDirectoryActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                startActivity(intent);
            }
        });


        tv_central = (TextView) findViewById(R.id.tv_central);
        tv_central.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "8108001";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(BasesDirectoryActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                startActivity(intent);
            }
        });

        tv_ecoradios = (TextView) findViewById(R.id.tv_ecoradios);
        tv_ecoradios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "8108001";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(BasesDirectoryActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                startActivity(intent);
            }
        });


        tv_Paseo = (TextView) findViewById(R.id.tv_paseo);
        tv_Paseo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "8295040";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                if (ActivityCompat.checkSelfPermission(BasesDirectoryActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.right_in,R.anim.right_out);
    }
}
