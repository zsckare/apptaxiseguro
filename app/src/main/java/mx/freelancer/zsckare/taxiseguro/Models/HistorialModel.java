package mx.freelancer.zsckare.taxiseguro.Models;

/**
 * Created by katty on 23/01/17.
 */

public class HistorialModel {
    String referencia, date, driver_name,driver_paterno,driver_materno,driver_economico;

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_paterno() {
        return driver_paterno;
    }

    public void setDriver_paterno(String driver_paterno) {
        this.driver_paterno = driver_paterno;
    }

    public String getDriver_materno() {
        return driver_materno;
    }

    public void setDriver_materno(String driver_materno) {
        this.driver_materno = driver_materno;
    }

    public String getDriver_economico() {
        return driver_economico;
    }

    public void setDriver_economico(String driver_economico) {
        this.driver_economico = driver_economico;
    }
    public String getDriverFullname() {
        String fullname ="";
        fullname = driver_name+" "+driver_materno+" "+driver_materno;
        return fullname;
    }
}
