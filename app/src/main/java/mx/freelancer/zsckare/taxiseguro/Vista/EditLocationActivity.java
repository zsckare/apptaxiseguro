package mx.freelancer.zsckare.taxiseguro.Vista;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.renderscript.Double2;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Locale;

import mx.freelancer.zsckare.taxiseguro.Api.Api;
import mx.freelancer.zsckare.taxiseguro.Models.LocationModel;
import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Utils.Comun;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class EditLocationActivity extends AppCompatActivity {

    private EditText referencesField;

    GoogleMap mMap;
    CameraUpdate cam;
    static String latlng = "";
    LatLng current_position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);

        referencesField = (EditText)findViewById(R.id.edit_referencias);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_edit);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateFavorite();
            }
        });
        setUpMap();
    }

    private void setUpMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapaedit);
            mMap = mapFragment.getMap();
            if (mMap != null) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMyLocationEnabled(false);


                latlng = LocationsActivity.favoritesServices.get(Comun.favorite_position).getLatlng();
                String coordenadas[] = latlng.split(",");
                double lat = Double.parseDouble(coordenadas[0]);
                double lng = Double.parseDouble(coordenadas[1]);
                cam = CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 18);
                mMap.animateCamera(cam);
                String address = getCompleteAddressString(lat,lng);
                referencesField.setText(address);




                mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        current_position = cameraPosition.target;
                        double lat = current_position.latitude;
                        double lng = current_position.longitude;
                        latlng = lat+","+lng;
                        String address = getCompleteAddressString(current_position.latitude,current_position.longitude);
                        referencesField.setText(address);
                    }
                });
            }
        }
    }



    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("Myaddress", "" + strReturnedAddress.toString());
            } else {
                Log.w("Myaddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Myaddress", "Canont get Address!");
        }
        return strAdd;
    }

    private void updateFavorite(){
        LocationModel service = LocationsActivity.favoritesServices.get(Comun.favorite_position);
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(Comun.BASE_URL)
                .build();


        Api api = adapter.create(Api.class);
        api.updateFavorite(service.getId(), latlng, service.getName(), referencesField.getText().toString(), new Callback<LocationModel>() {
            @Override
            public void success(LocationModel locationModel, Response response) {
                Toast.makeText(EditLocationActivity.this, "Editado", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(EditLocationActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
