package mx.freelancer.zsckare.taxiseguro.Utils;


import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Comun {
    public static final String BASE_URL = "http://69.87.217.79/api/v1/";
    //public static final String BASE_URL = "https://still-eyrie-51082.herokuapp.com/api/v1/";
    public static int driver_id = 0;
    public static String authentication_token = "";
    public static final String keyprivate ="@aplication_wcode(encrypt)@";
    public static int chat_id = 0;
    public static int service_id = 0;
    public static String latlng = "";
    public static String MD5 = "MD5";
    public static String SHA256 = "SHA-256";
    public static int favorite_position = 0;
    public static int logout = 0;
    private static String toHexadecimal(byte[] digest){
        String hash = "";
        for(byte aux : digest) {
            int b = aux & 0xff;
            if (Integer.toHexString(b).length() == 1) hash += "0";
            hash += Integer.toHexString(b);
        }
        return hash;
    }

    public static String getStringMessageDigest(String message, String algorithm){
        byte[] digest = null;
        byte[] buffer = message.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.reset();
            messageDigest.update(buffer);
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
            Log.e("Error","Error creando Digest");
        }
        return toHexadecimal(digest);
    }

    public static String getToken(String str){
        String tkn;
        String clvprivada ="@aplication_wcode(encrypt)@";
        String id = clvprivada+str;
        tkn= getStringMessageDigest(id,SHA256);
        return tkn;
    }

    public static int verifySesion(int estado){
        int status = estado;
        return status;
    }

    final static String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();

    public static String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

}
