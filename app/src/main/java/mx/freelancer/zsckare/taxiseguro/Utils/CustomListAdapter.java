package mx.freelancer.zsckare.taxiseguro.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import mx.freelancer.zsckare.taxiseguro.R;
import mx.freelancer.zsckare.taxiseguro.Vista.DrawerActivity;
import mx.freelancer.zsckare.taxiseguro.Vista.HistorialActivity;

/**
 * Created by katty on 24/01/17.
 */

public class CustomListAdapter extends ArrayAdapter<String> {
    String TAG = getClass().getName();
    public CustomListAdapter(Context context, LinkedList<String> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtener inflater.
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // ¿Existe el view actual?
        if (null == convertView) {
            convertView = inflater.inflate(
                    R.layout.custom_list_item,
                    parent,
                    false);
        }

        // Referencias UI.
        TextView ref = (TextView) convertView.findViewById(R.id.service_title);
        TextView date = (TextView) convertView.findViewById(R.id.service_date);

        // Lead actual.
        //Lead lead = getItem(position);
        String title = HistorialActivity.historialServices.get(position).getReferencia();
        String fecha = HistorialActivity.historialServices.get(position).getDate();
        // Setup.

        // Glide.with(getContext()).load(lead.getImage()).into(avatar);
        ref.setText(title);
        date.setText(fecha);

        return convertView;
    }
}